module.exports = {
  bracketSpacing: false,
  singleQuote: false,
  bracketSameLine: true,
  trailingComma: "all",
  arrowParens: "avoid",
  endOfLine: "auto",
};
