import "../../styles/components/Button.css";

export const Button = ({ onClick, children }: { onClick: () => void, children: any }) => {
    return <button onClick={() => onClick()}>
        {children}
    </button>;
};
