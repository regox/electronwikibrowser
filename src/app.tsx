import { useState } from "react";
import { createRoot } from "react-dom/client";

import { Button } from "./components/atoms/Button";

const MainComponent = () => {
    const [number, setNumber] = useState(0);
    return (
        <>
            <h1>ElectronWikiBrowser</h1>
            <p>Log in to get started.</p>
            <p>{number}</p>
            <Button onClick={() => setNumber(number + 1)}>
                Log in
            </Button>
        </>
    );
};
const root = createRoot(document.getElementsByClassName("coreContainer")[0]);
root.render(<MainComponent />);
